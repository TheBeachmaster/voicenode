## Setup 

### Bare 
+ `npm install --save` 
+ `npm start` 

Your app will run on the given port. 

You can override the default port by running 
`PORT=<DESIREDPORTNUMBER> npm start` 

The routes will be you call back url

Read within the app...


## Docker 

+ `docker build -t myvoiceapp .`  
+ `docker run -it myvoiceapp` 
 
## Docker-compose 

+ `docker-compose build` 
+ `docker-compose up -d` 
