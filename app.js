const file = require('fs');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const url = require('url');
const http = require('http');
const morgan = require('morgan');
const path = require('path');

const filePath = "./log/evt.log";
const filePathDtmf = "./log/dtmf.log";
const recordingLogs = "./log/recording.log";
const callBackUrl = ""; // A URL where event data will be sent
let callActions,responseAction = "";
const recordingDir = path.join(__dirname, 'records');
const loggingingDir = path.join(__dirname, 'log');

file.existsSync(loggingingDir) || file.mkdirSync(loggingingDir);
file.existsSync(recordingDir) || file.mkdirSync(recordingDir);

const opts = {
    encoding:'utf8',
    flags:'a'
}

app.use(bodyParser.json());
app.use(express.static(__dirname + '/static')); // Serve our AUDIO file (HOST:PORT/audio.mp3)
let accessLogStream = file.createWriteStream(path.join(__dirname, './log/app.morgan.log'), {flags: 'a'});
app.use(morgan('common', {stream: accessLogStream}));

app.get('/', (req, res) => {
    res.send('Am Alive');
});

const callerId = ''; // your Africa's Talking Virtual Number
let calleephoneNum= []; // Who do you want to call... Could be one number or an array of numbers
let calleeSipAddr = ''; // something like test.user@<country>.sip.africastalking.com : test.user.ke.sip.africastalking.com
const fileHostUrl = ''; // something like http://host:file.extension

app.post('/voice/multi/saydial', (req, res) => {
    console.log(req.body);
    callActions = '<Say> This is a test service </Say>';
    callActions += `<Dial phoneNumbers="${calleephoneNum}" callerId="${callerId}" />`;
    responseAction = '<?xml version="1.0" encoding="UTF-8"?><Response>'+`${callActions}`+'</Response>';
    res.send(responseAction);
});

app.post('/voice/sip/dial', (req, res) => {
    console.log(req.body);
    callActions = `<Dial phoneNumbers="${calleeSipAddr}" />`;
    responseAction = '<?xml version="1.0" encoding="UTF-8"?><Response>'+`${callActions}`+'</Response>';
    res.send(responseAction);
});

app.post('/voice/dial', (req, res) => {
    console.log(JSON.stringify(req.body, null, 2));
    callActions = `<Dial phoneNumbers="${calleephoneNum}" callerId="${callerId}"/>`;
    responseAction = '<?xml version="1.0" encoding="UTF-8"?><Response>'+`${callActions}`+'</Response>';
    res.send(responseAction);
});

app.post('/voice/say', (req, res) => {
    console.log(JSON.stringify(req.body, null, 2));
    let callerNumber = req.body['callerNumber'];
    console.log(callerNumber);
    callActions = '<Say voice="woman" playbeep="true"> Hello ' + `${callerNumber}`+ '. I see you</Say>';
    responseAction = '<?xml version="1.0" encoding="UTF-8"?><Response>'+`${callActions}`+'</Response>';
    res.send(responseAction);
});

app.post('/voice/play', (req, res) => {
    console.log(JSON.stringify(req.body, null, 2));
    let audioUrl = fileHostUrl;
    callActions = '<Say voice="woman">Playing your audio file</Say>';
    callActions += `<Play url="${audioUrl}"/>`;
    responseAction = '<?xml version="1.0" encoding="UTF-8"?><Response>'+`${callActions}`+'</Response>';
    res.send(responseAction);
});

app.post('/voice/dtmf', (req, res) =>{
    console.log(JSON.stringify(req.body, null, 2));
    callActions = `<GetDigits numDigits="4" callbackUrl="${callBackUrl}" timeout="30" finishOnKey="#">`;
    callActions += '<Say voice="woman">Please Enter your Password to proceed. Press the pound sign to finish.</Say>';
    callActions += '</GetDigits>';
    callActions += '<Say voice="woman">Am sorry, We did not get that.</Say>';
    responseAction = '<?xml version="1.0" encoding="UTF-8"?><Response>'+`${callActions}`+'</Response>';
    res.send(responseAction);
});

app.post('/voice/record', (req, res) =>{
    console.log(JSON.stringify(req.body, null, 2));
    callActions = `<Record callbackUrl="${callBackUrl}" timeout="10" finishOnKey="#">`;
    callActions += '<Say voice="woman">Please say your Password to proceed. Press the pound sign to finish.</Say>';
    callActions += '</Record>';
    callActions += '<Say voice="woman">Am sorry, We did not get that.</Say>';
    responseAction = '<?xml version="1.0" encoding="UTF-8"?><Response>'+`${callActions}`+'</Response>';
    res.send(responseAction);
});

app.post('/voice/sayplayrecord', (req, res) =>{
    console.log(JSON.stringify(req.body, null, 2));
    let audioUrl = fileHostUrl;
    callActions = '<Say voice="woman"> Please record your message after the music. </Say>';
    callActions += `<Play url="${audioUrl}" />`;
    callActions += `<Record callbackUrl="${callBackUrl}" timeoout="10" finishOnKey="#"></Record>`;
    responseAction = '<?xml version="1.0" encoding="UTF-8"?><Response>'+`${callActions}`+'</Response>';
    res.send(responseAction);
});

// Stores recorded calls

app.post('/voice/events/records', (req, res) =>{
    file.writeFile(recordingLogs, "===LOG BEGIN===" + "\n", opts, (err) => {
        if(err) throw err;
    });
    file.open(recordingLogs,'a', (err, fd) =>{
        if(err) throw err;
        file.appendFile(fd, JSON.stringify(req.body, null, 2), opts, (err) => {
            file.close(fd, (err) => {
                if(err) throw err;
            });
            if(err) throw err;
        });
    });
    console.log("Contains recording: "+ req.body.hasOwnProperty('recordingUrl'))
    if(req.body.hasOwnProperty('recordingUrl'))
    {
        let recordingFileUrl = req.body['recordingUrl'];
        console.log(recordingFileUrl);
        let recordingFileName = url.parse(recordingFileUrl).pathname.split('/').pop();
        let createFile = file.createWriteStream(recordingDir + "/" + recordingFileName);
        var fileRequest = http.get(recordingFileUrl, function(response) {
        response.pipe(createFile);
        });
        console.log(fileRequest);
    }
    callActions = '<Say voice="woman" playbeep="true"> Hello ' + `${req.body['callerNumber']}`+ '. Thank You</Say>';
    responseAction = '<?xml version="1.0" encoding="UTF-8"?><Response>'+`${callActions}`+'</Response>';
    res.send(responseAction);    
    //res.status(200).send('Ok');
});

// All voice events will be sent here... Your events callback
app.post('/voice/events',(req, res) => {
    file.writeFile(filePath, "===LOG BEGIN==="+ "\n", opts ,(err) => {
        if(err) throw err;
    });
    file.open(filePath,'a', (err, fd) =>{
        if(err) throw err;
        file.appendFile(fd, JSON.stringify(req.body, null, 2), opts, (err) => {
            file.close(fd, (err) => {
                if(err) throw err;
            });
            if(err) throw err;
        });
    });
    res.status(200).send('Ok');
});

// This will write dtmf data to file... Should be saved in a DB ideally

app.post('/voice/dtmf/data', (req, res) => {
    file.writeFile(filePathDtmf, "===DTMF BEGIN===" + "\n", opts ,(err) => {
        if(err) throw err;
    });
    file.open(filePathDtmf,'a', (err, fd) =>{
        if(err) throw err;
        file.appendFile(fd, JSON.stringify(req.body, null, 2), opts, (err) => {
            file.close(fd, (err) => {
                if(err) throw err;
            });
            if(err) throw err;
        });
    });
    console.log(JSON.stringify(req.body, null, 2));
    let callerNumber = req.body['callerNumber'];
    console.log(callerNumber);
    callActions = '<Say voice="woman" playbeep="true"> Hello ' + `${callerNumber}`+ '. Thank You</Say>';
    responseAction = '<?xml version="1.0" encoding="UTF-8"?><Response>'+`${callActions}`+'</Response>';
    res.send(responseAction);    
    //res.status(200).send('Ok');
});

// Starts the app

app.listen(process.env.PORT || 3080, (server) => {
    console.log('We are up!');
});